@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <span style="float: left">
                        <h5>Add New Employee</h5>
                    </span>
                   <span style="float: right">
                    {{-- <a href="{{route('add-new-employee')}}">
                        <button type="button" class="btn btn-secondary">Add New Employee</button>
                    </a> --}}
                        
                   </span>
                </div>
                

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{route('save-employee')}}">
                        @csrf
                        <div class="row">
                            <div class="mb-3 col-md-6">
                                <label for="" class="form-label">First Name</label>
                                <input type="text" class="form-control" name="first_name" required>
                               
                            </div>
                            <div class="mb-3 col-md-6">
                                  <label for="" class="form-label">Last Name</label>
                                  <input type="text" class="form-control" name="last_name" required>
                                  
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-3 col-md-6">
                                <label for="" class="form-label">Company</label>
                                <select class="form-select" aria-label="Choose a company" name="company">
                                    <option selected disabled>Please Select a company</option>
                                    @foreach ($companies as $company)
                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                   
                                  </select>
                               
                            </div>
                            <div class="mb-3 col-md-6">
                                  <label for="" class="form-label">Departments</label>
                                  <input type="text" class="form-control" name="department">
                                  
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="mb-3 col-md-4">
                                <label for="exampleInputEmail1" class="form-label">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
                                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                                
                          </div>
                            <div class="mb-3 col-md-4">
                                <label for="" class="form-label">Phone</label>
                                <input type="text" class="form-control" name="phone" required>
                               
                            </div>
                            <div class="mb-3 col-md-4">
                                <label for="" class="form-label">Staff ID</label>
                                <input type="number" class="form-control" name="staff_id">

                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="mb-3 col-md-6">
                                <label for="" class="form-label">Password</label>
                                <input type="password" class="form-control" name="password" id="txtPassword" required>
                               
                            </div>
                            <div class="mb-3 col-md-6">
                                  <label for="" class="form-label">Confirm Password</label>
                                  <input type="password" class="form-control" name="re_password" id="txtConfirmPassword" required>
                                  
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-3 col-md-4">
                                <label for="" class="form-label">Address</label>
                                <textarea id="" name="address" rows="4" cols="50"></textarea>
                                
                          </div>
                        </div>
                        
                       
                        <button type="submit" class="btn btn-primary" onclick="return Validate()">Submit</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function Validate() {
        var password = document.getElementById("txtPassword").value;
        var confirmPassword = document.getElementById("txtConfirmPassword").value;
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }
</script>
@endsection