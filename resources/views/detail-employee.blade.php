@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <span style="float: left">
                        <h5>Employee Detail</h5>
                    </span>
                   <span style="float: right">
                    {{-- <a href="{{route('add-new-employee')}}">
                        <button type="button" class="btn btn-secondary">Add New Employee</button>
                    </a> --}}
                        
                   </span>
                </div>
                

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form>
                        @csrf
                        <div class="row">
                            <div class="mb-3 col-md-6">
                                <label for="" class="form-label">First Name</label>
                                <input type="text" class="form-control" value="{{$employee->first_name}}" disabled>
                               
                            </div>
                            <div class="mb-3 col-md-6">
                                  <label for="" class="form-label">Last Name</label>
                                  <input type="text" class="form-control" name="last_name" value="{{$employee->last_name}}" disabled>
                                  
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-3 col-md-6">
                                <label for="" class="form-label">Company</label>
                                <select class="form-select" aria-label="Choose a company" name="company">
                                    
                                    @foreach ($companies as $company)
                                    @if ($company->id == $employee->company_id)
                                        <option value="{{$company->id}}" disabled selected>{{$company->name}}</option>
                                    @endif
                                        
                                    @endforeach
                                   
                                  </select>
                               
                            </div>
                            <div class="mb-3 col-md-6">
                                  <label for="" class="form-label">Departments</label>
                                  <input type="text" class="form-control" name="department" value="{{$employee->department}}" disabled>
                                  
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="mb-3 col-md-4">
                                <label for="exampleInputEmail1" class="form-label">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" value="{{$employee->email}}" disabled>
                                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                                
                          </div>
                            <div class="mb-3 col-md-4">
                                <label for="" class="form-label">Phone</label>
                                <input type="text" class="form-control" name="phone" value="{{$employee->phone}}" disabled>
                               
                            </div>
                            <div class="mb-3 col-md-4">
                                <label for="" class="form-label">Staff ID</label>
                                <input type="number" class="form-control" name="staff_id" value="{{$employee->staff_id}}" disabled>

                            </div>
                            
                        </div>
                       
                        <div class="row">
                            <div class="mb-3 col-md-4">
                                <label for="" class="form-label">Address</label>
                                <textarea id="" name="address" rows="4" cols="50" readonly>
                                    {{$employee->address}}
                                </textarea>
                                
                          </div>
                        </div>
                        
                       
                      
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function Validate() {
        var password = document.getElementById("txtPassword").value;
        var confirmPassword = document.getElementById("txtConfirmPassword").value;
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }
</script>
@endsection