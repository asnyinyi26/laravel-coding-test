@extends('layouts.app')

@section('content')
@php
    $permission = Auth::user();
@endphp
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                
                <div class="card-header">
                    <span style="float: left">
                        <h5>Employees</h5>
                    </span>
                   <span style="float: right">
                    <a href="{{route('add-new-employee')}}">
                        <button type="button" class="btn btn-secondary">Add New Employee</button>
                    </a>
                        
                   </span>
                </div>
                <div class="card-header">
                    <form action="{{route('employee')}}" method="GET">
                        <div class="row">
                            <div class="mb-3 col-md-3">
                                <label for="" class="form-label">Company</label>
                                @if ($_GET['company']??null)
                                <select class="form-select" aria-label="Choose a company" name="company">
                                    @foreach ($companies as $company)
                                        @if ($_GET['company'] == $company->id)
                                            <option value="{{$company->id}}" selected>{{$company->name}}</option>
                                        @else
                                            <option value="{{$company->id}}">{{$company->name}}</option>
                                        @endif
                                       
                                    @endforeach
                                   
                                  </select>
                                @else 
                                <select class="form-select" aria-label="Choose a company" name="company">
                                    <option selected disabled value="0">Please Select a company</option>
                                    @foreach ($companies as $company)
                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                   
                                  </select>
                                @endif
                            </div>
                                <div class="mb-3 col-md-2">
                                    <label for="" class="form-label">Department Name</label>
                                    @if ($_GET['department']??null)
                                        <input type="text" class="form-control" name="department" value="{{$_GET['department']}}">
                                    @else
                                        <input type="text" class="form-control" name="department" value="">
                                    @endif
                                   
                                   
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label for="" class="form-label">Employee Name</label>
                                    @if ($_GET['name']??null)
                                        <input type="text" class="form-control" name="name" value="{{$_GET['name']}}">
                                    @else
                                        <input type="text" class="form-control" name="name" value="">
                                    @endif
                                   
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label for="" class="form-label">Staff ID</label>
                                    @if ($_GET['staff_id']??null)
                                        <input type="text" class="form-control" name="staff_id" value="{{$_GET['staff_id']}}">
                                    @else 
                                        <input type="text" class="form-control" name="staff_id" value="">
                                    @endif
                                   
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label for="" class="form-label">Search</label>
                                    <div>
                                        <button type="submit" class="btn btn-info mr-2">Search</button>
                                        <a href="{{route('employee')}}">
                                            <button type="button" class="btn btn-warning">Clear</button>
                                        </a>
                                        
                                    </div>
                                    
                                        
                                   
                                   
                                </div>
                            
                        </div>
                    </form>
                    
                </div>
                

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped" id="tblData">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Company</th>
                            <th scope="col">phone</th>
                            <th>
                                <button type="button" class="btn btn-success" onclick="exportTableToExcel('tblData')">Export</button>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          
                              @foreach ($employees as $employee)
                              <tr>
                              @php
                                  $name = App\Company::find($employee->company_id)
                              @endphp
                                <th scope="row">{{$employee->id}}</th>
                                <td>{{$employee->first_name}}</td>
                                <td>{{$employee->last_name}}</td>
                                <td>{{optional($name)->name}}</td>    
                                <td>{{$employee->phone}}</td>
                                @if ($permission->employee != 1)
                                <td>
                                    <a href="{{route('detail-employee')}}?id={{$employee->id}}" target="_blank">
                                        <button type="button" class="btn btn-info">Details</button>
                                    </a>
                                    <a href="{{route('edit-employee')}}?id={{$employee->id}}">
                                        <button type="button" class="btn btn-success">Edit</button>
                                    </a>
                                    <a href="{{route('delete-employee')}}?id={{$employee->id}}" onclick="confirm()">
                                        <button type="button" class="btn btn-danger">Delete</button>
                                    </a>
                                </td>
                                @else 
                                 <td>

                                 </td>
                                @endif
                                
                            </tr>
                              @endforeach
                            
                           
                          
                         
                        </tbody>
                        {{ $employees->links() }}
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}
</script>
@endsection