@extends('layouts.app')

@section('content')
@php
        $permission = Auth::user();
@endphp
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card-header">
                    <span style="float: left">
                        <h5>Companies</h5>
                    </span>
                   <span style="float: right">
                    <a href="{{route('add-new-company')}}">
                        <button type="button" class="btn btn-secondary">Add New Company</button>
                    </a>
                        
                   </span>
                </div>
                

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped" id="tblData">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Address</th>
                            <th>
                                <button type="button" class="btn btn-success" onclick="exportTableToExcel('tblData')">Export</button>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                              @foreach ($companies as $company)
                              <tr>
                                <th scope="row">{{$company->id}}</th>
                                <td>{{$company->name}}</td>
                                <td>{{$company->email}}</td>
                                <td>{{$company->address}}</td>
                                @if ($permission->employee != 1)
                                <td>
                                    <a href="{{route('detail-company')}}?id={{$company->id}}" target="_blank">
                                        <button type="button" class="btn btn-info">Details</button>
                                    </a>
                                    <a href="{{route('edit-company')}}?id={{$company->id}}">
                                        <button type="button" class="btn btn-success">Edit</button>
                                    </a>
                                    <a href="{{route('delete-company')}}?id={{$company->id}}" onclick="confirm()">
                                        <button type="button" class="btn btn-danger">Delete</button>
                                    </a>
                                   
                                </td>
                                @else 
                                    <td>

                                    </td>
                                @endif
                              </tr>
                               
                              @endforeach
                            
                           
                          </tr>
                         
                        </tbody>
                        {{ $companies->links() }}
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}
</script>
@endsection