@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <span style="float: left">
                        <h5>Add New Company</h5>
                    </span>
                   <span style="float: right">
                    {{-- <a href="{{route('add-new-employee')}}">
                        <button type="button" class="btn btn-secondary">Add New Employee</button>
                    </a> --}}
                        
                   </span>
                </div>
                

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{route('save-company')}}">
                        @csrf
                        <div class="row">
                            <div class="mb-3 col-md-6">
                                <label for="" class="form-label">Company Name</label>
                                <input type="text" class="form-control" name="name" required>
                               
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="exampleInputEmail1" class="form-label">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" required>
                                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                                
                          </div>
                        </div>
                        
                        <div class="row">
                            <div class="mb-3 col-md-4">
                                <label for="" class="form-label">Address</label>
                                <textarea id="" name="address" rows="4" cols="50" required></textarea>
                                
                          </div>
                        </div>
                        
                       
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection