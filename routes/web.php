<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Company
Route::get('/company','CompanyController@index')->name('company');
Route::get('/add-new-company','CompanyController@create')->name('add-new-company');
Route::post('/save-company','CompanyController@save')->name('save-company');
Route::get('/detail-company','CompanyController@detail')->name('detail-company');
Route::get('/edit-company','CompanyController@edit')->name('edit-company');
Route::get('/delete-company','CompanyController@delete')->name('delete-company');
Route::post('/save-edit-company','CompanyController@save_edit')->name('save-edit-company');

//Employee
Route::get('/employee','EmployeeController@index')->name('employee');
Route::get('/add-new-employee','EmployeeController@create')->name('add-new-employee');
Route::post('/save-employee','EmployeeController@save')->name('save-employee');
Route::get('/detail-employee','EmployeeController@detail')->name('detail-employee');
Route::get('/edit-employee','EmployeeController@edit')->name('edit-employee');
Route::get('/delete-employee','EmployeeController@delete')->name('delete-employee');
Route::post('/save-edit-employee','EmployeeController@save_edit')->name('save-edit-employee');
