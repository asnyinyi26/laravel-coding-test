<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Employee;
use App\Company;
use DB;
use Hash;

class EmployeeController extends Controller
{
    public function index(request $request){
        //dd($request);
        if($request->has('company') || $request->has('department') || $request->has('name') || $request->has('staff_id')){
            //dd($request);
            $employees = Employee::where(function ($query) use ($request) {
                                    if($request->has('company') && isset($request->company)){
                                        $query->where('company_id',$request->company);
                                    }
                                    if($request->has('department') && isset($request->department)){
                                        $query->where('department',$request->department);
                                    }
                                    if($request->has('name') && isset($request->name)){
                                        $query->where('first_name',$request->name);
                                        $query->orwhere('last_name',$request->name); 
                                    }
                                    if($request->has('staff_id') && isset($request->staff_id)){
                                        $query->where('staff_id',$request->staff_id);
                                    }
                                })
                               ->paginate(10);
            $companies = Company::All();                   
        }
        else{
            $employees =DB::table('employees')->paginate(10);
            $companies = Company::All();
        }
        return view('/employee',['employees'=>$employees,'companies'=>$companies]);
    }

    public function create(request $request){
        $companies = Company::All();
        return view('/add-new-employee',['companies'=>$companies]);
    }

    public function save(request $request){
        $validated = $request->validate([
            'email' => 'required|unique:employees|max:255',
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        $user = new User;
        $user->name = $request->first_name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->employee = 1;
        $user->save();

        if($user->save()){
            $employee = new Employee;
            $employee->first_name = $request->first_name;
            $employee->last_name = $request->last_name;
            $employee->company_id = $request->company;
            $employee->department = $request->department;
            $employee->email = $request->email;
            $employee->phone = $request->phone;
            $employee->staff_id = $request->staff_id;
            $employee->address = $request->address;
            $employee->user_id = $user->id;
            $employee->save();

            if($employee->save()){
                return redirect()->route('employee')->with('message', 'Suessfully Added a employee');
            }
        }
    }

    public function detail(request $request){
        $employee = Employee::find($request->id);
        $companies = Company::All();
        return view('detail-employee',['employee'=>$employee,'companies'=>$companies]);
    }

    public function edit(request $request){
        $employee = Employee::find($request->id);
        $companies = Company::All();
        return view('edit-employee',['employee'=>$employee,'companies'=>$companies]);
    }

    public function delete(request $request){
        $employee = Employee::find($request->id);
        User::find($employee->user_id)->delete();
        $employee->delete();
        return redirect()->route('employee')->with('message', 'Suessfully Deleted');
    }

    public function save_edit(request $request){
            $employee = Employee::find($request->employee_id);
            $employee->first_name = $request->first_name;
            $employee->last_name = $request->last_name;
            $employee->company_id = $request->company;
            $employee->department = $request->department;
            $employee->email = $request->email;
            $employee->phone = $request->phone;
            $employee->staff_id = $request->staff_id;
            $employee->address = $request->address;
            $employee->save();

            if($employee->save()){
                $user = User::find($employee->user_id);
                $user->name = $employee->first_name;
                $user->email = $employee->email;
                $user->save();

                return redirect()->route('employee')->with('message', 'Suessfully Edited');
            }
    }
}
