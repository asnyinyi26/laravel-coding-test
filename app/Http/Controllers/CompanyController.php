<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use DB;

class CompanyController extends Controller
{
    public function index(request $request){
        $companies =DB::table('companies')->paginate(10);
        return view('/company',['companies'=>$companies]);
    }

    public function create(request $request){
        return view('/add-new-company');
    }

    public function save(request $request){
        $validated = $request->validate([
            'email' => 'required|unique:companies|max:255',
            'name' => 'required',
            'address' => 'required',
        ]);

        $company = new Company;
        $company->name = $request->name;
        $company->email = $request->email;
        $company->address = $request->address;
        $company->save();

        if($company->save()){
            $companies = Company::All();
            return redirect()->route('company')->with('message', 'Suessfully Added a company');
        }
    }

    public function detail(request $request){
        $company = Company::find($request->id);
        return view('detail-company',['company'=>$company]);
    }

    public function edit(request $request){
        $company = Company::find($request->id);
        return view('edit-company',['company'=>$company]);
    }

    public function delete(request $request){
        Company::find($request->id)->delete();
        return redirect()->route('company')->with('message', 'Suessfully Deleted');
    }

    public function save_edit(request $request){
        $company = Company::find($request->company_id);
        $company->name = $request->name;
        $company->email = $request->email;
        $company->address = $request->address;
        $company->save();

        return redirect()->route('company')->with('message', 'Suessfully Edited');
    }
}
